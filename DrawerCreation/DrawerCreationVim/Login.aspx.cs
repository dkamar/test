﻿using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace VimRegistrationWebApp
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            bool AuthStatus = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["EnableWinAuth"]);
            if (AuthStatus)
            {
                if (Context.User.Identity.IsAuthenticated) Response.Redirect("~/Home/About");
            }
            else
                Response.Redirect("~/Home/About");
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            string username = txtUserName.Text.Trim();
            if (username.Contains("\\")) username = username.Split('\\')[1].Trim();

            if (IsAuthenticated("Vertafore", username, txtPassword.Text))
            {

                HttpCookie authCookie = FormsAuthentication.GetAuthCookie(username, false);

                FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(authCookie.Value);

                DateTime expirationTime = DateTime.Now.AddHours(8);

                FormsAuthenticationTicket newTicket = new FormsAuthenticationTicket(ticket.Version, ticket.Name, ticket.IssueDate, expirationTime, ticket.IsPersistent, username);

                authCookie.Value = FormsAuthentication.Encrypt(newTicket);

                Response.Cookies.Add(authCookie);

                Response.Redirect("~/Home/About");
            }
            else
                lblMsg.Text = "Invalid credentials!!";
                lblMsg.Style.Add("display", "show");
        }

        public bool IsAuthenticated(string domain, string username, string pwd)
        {
            string _path = System.Configuration.ConfigurationManager.AppSettings["LDAPPath"] as string;

            string domainAndUsername = domain + @"\" + username;
            DirectoryEntry entry = new DirectoryEntry(_path, domainAndUsername, pwd);

            try
            {
                //Bind to the native AdsObject to force authentication.
                object obj = entry.NativeObject;

                DirectorySearcher search = new DirectorySearcher(entry);

                search.Filter = "(SAMAccountName=" + username + ")";
                search.PropertiesToLoad.Add("cn");
                SearchResult result = search.FindOne();

                if (null == result)
                {
                    return false;
                }

                //Update the new path to the user in the directory.
                //_path = result.Path;
                //_filterAttribute = (string)result.Properties["cn"][0];
            }
            catch (Exception ex)
            {
                //throw new Exception("Error authenticating user. " + ex.Message);
                return false;
            }

            return true;
        }
    }
}