﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace DrawerCreationVim.Models
{
    [Bind]
    public class CreateDrawer
    {
        [Required(ErrorMessage = "VID is Required")]
        [RegularExpression(@"[0-9]+-[0-9]", ErrorMessage = "Incorrect GTID Format")]
        public string Vid { get; set; }

        [Required(ErrorMessage = "Agency Name is Required")]
        [StringLength(50, MinimumLength = 3)]
        public string Name { get; set; }

        [Required(ErrorMessage = "GTID is Required")]
        [RegularExpression(@"[a-zA-Z0-9]+ *.-[a-zA-Z0-9]+ *.-[a-zA-Z0-9]+ *.-[a-zA-Z0-9]+ *.-[a-zA-Z0-9]+ *", ErrorMessage = "Incorrect GTID Format")]
        public string Gtid { get; set; }
    }
}