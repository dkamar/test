﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="VimRegistrationWebApp.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>VSSO Relying Party Registration - Login</title>
</head>
<body style="font-family: verdana, arial, helvetica, sun-sans, sans-serif; font-size: small;">
<center>

        <table width="100%" height="50px" cellpadding="5" cellspacing="0" border="0" style=" background-color: #fafafa; border-spacing: 0; color: #282828; font-family: Source Sans Pro, sans-serif; "bgcolor="#fafafa;" >
            <tr bgcolor="inherit><td width="100%" bgcolor="inherit"></td></tr>
        </table>

        <table width="100%" cellpadding="0" cellspacing="0"  border="0" style=" background-color: #fafafa; border-spacing: 0; color: #282828; font-family: Source Sans Pro, sans-serif; " bgcolor="#d9e4ec;" >
            <tr><td width="100%">
        
                    <div class="webkit">
                        <table class="outer">
                            <tr><td class="no-padding">
                                <table class="one-column"  align="center" cellpadding="0" cellspacing="0" style="border-top:8px solid #EC611A;">
                                    <tr><td bgcolor="#ffffff" width="600" align="center" valign="top" style="text-align: center;">
                                        <table cellpadding="30" cellspacing="0" border="0">
                                            <tr><td class="content" align="left">


                                                <!-- Page Content -->
                                                <form id="form1" runat="server">

                                                    <table border="0" cellspacing="0" width="100%" style=" border-spacing: 0; color: #282828; font-family: Source Sans Pro, sans-serif;">
                                                        <tr><td><asp:Image Id="Image1" ImageUrl="~/Content/images/Vertafore-Full_Color-RGB.png" runat="server"/></td></tr>
                                                    </table>

                                                    <table cellpadding="0" cellspacing="0" border="0" style=" border-spacing: 0; bgcolor: #282828; font-family: Source Sans Pro, sans-serif;">
                                                        <tr><td height="20" width="540" style=" font-size: 20px; line-height: 20px;"></td></tr>
                                                    </table>

                                                    <table>
                                                        <tr>
                                                            <td><p class="bold" style="text-align: center;">Welcome to the VSSO Relying Party Registration</p></td>
                                                        </tr>
                                                    </table>

                                                    <table cellpadding="0" cellspacing="0" border="0" style=" border-spacing: 0; bgcolor: #282828; font-family: Source Sans Pro, sans-serif;">
                                                        <tr><td height="20" width="540" style=" font-size: 20px; line-height: 20px;"></td></tr>
                                                    </table>

                                                    <table width="100%">
                                                        <tr>
                                                            <td width="180px"><p class="auto-style1">Username:</p></td>
                                                            <td><asp:TextBox ID="txtUserName" runat="server" Width="100%"></asp:TextBox></td>
                                                        </tr>
                                                        <tr>
                                                            <td width="180px"><p>Password:</p></td>
                                                            <td><asp:TextBox ID="txtPassword" runat="server" TextMode="Password" Width="100%"></asp:TextBox></td>
                                                        </tr>
                                                        <tr>
                                                            <td width="180px"><p>Domain:</p></td>
                                                            <td><p>Vertafore</p></td>
                                                        </tr>
                                                        <tr>
                                                            <td></td>
                                                            <td><asp:Button runat="server" ID="btnLogin" Text="Sign In" OnClick="btnLogin_Click"/></td>
                                                        </tr>

                                                        <tr><td height="20" style=" font-size: 20px; line-height: 20px;"></td></tr>

                                                        <table>
                                                            <tr>
                                                                <td><asp:Label ID="lblMsg" runat="server" Text="" width="100%" style="padding: 5px 8px; background-color:red; color:white; display:none"></asp:Label></td>
                                                            </tr>
                                                        </table>
                                                        
                                                    </table>

                                                </form>


                                            </td></tr>
                                        </table>
                                    </td></tr>
                                </table>
                            </td></tr>
                        </table>
                    </div>
                </table>

        <table width="100%" cellpadding="5" cellspacing="0" border="0" style=" background-color: #fafafa; border-spacing: 0; color: #282828; font-family: Source Sans Pro, sans-serif; " bgcolor="#fafafa;" >
            <tr bgcolor="inherit><td width="100%" bgcolor="inherit"></td></tr>
        </table>
    </center>
</body>
    <style type="text/css">
        body {
            margin: 0;
            padding: 0;
            min-width: 100%;
            background-color: #fafafa;
            font-family: Source Sans Pro, sans-serif;
            -webkit-font-smoothing: antialiased;
        }
        center {
            width: 100%;
            table-layout: fixed;
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
            background-color: #d9e4ec;
        }
        .webkit {
            max-width: 600px;
            margin: 0 auto;
            border: 1px solid #d7d7d7;
            border-radius: 5px;
            overflow: hidden;
        }
        .outer {
            border: 0;
            border-spacing: 0;
            font-family: Arial, sans-serif;
            color: #333333;
            margin: 0 auto;
            width: 100%;
            max-width: 600px;
            background-color: #d9e4ec;
        }
        .one-column {
            width: "100%";
            border-spacing: 0;
            max-width: 600px;
            background-color: #ffffff;
            color: #282828;
            font-family: Source Sans Pro, sans-serif;
            background-color: #ffffff;
        }
        .no-padding {
            padding-top: 0;
            padding-bottom: 0;
            padding-right: 0;
            padding-left: 0;
        }
        .content {
            padding-top: 20px;
            padding-bottom: 30px;
            padding-right: 30px;
            padding-left: 30px;
            text-align: left;
            border-spacing: 0;
            color: #282828;
            font-family: Source Sans Pro, sans-serif;
        }
        #Image1 {
            width: 180px;
            height: 30px;
            border: 0;
        }
        p {
            text-align: left;
            font-size: 16px;
            font-family: Source Sans Pro, sans-serif;
            -webkit-font-smoothing: antialiased;
            line-height: 22px;
            margin: 0;
            padding-bottom: 10px;
            padding-left: 1px;
        }
        .bold {
            
            font-weight: bold;
        }
        #btnLogin {
            padding: 9px 32px;
            font-size: 16px;
            font-weight: bold;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            -ms-border-radius: 3px;
            border-radius: 3px;
            border: 0;
            font-family: Source Sans Pro, sans-serif;
            text-align: center;
            cursor: pointer;
            outline: none;
            background: #008cd2;
            width: 100%;
            color: white;
            font-weight: bold;
        }
        .auto-style1 {
            width: 173px;
        }
    </style>
</html>
