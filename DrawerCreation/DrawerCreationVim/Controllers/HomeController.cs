﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace DrawerCreationVim.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private IConfiguration configuration;
        ExecutePS createDrawer;
        public HomeController()
        {
            createDrawer = new ExecutePS(configuration);
        }
        public HomeController(ILogger<HomeController> logger, IConfiguration iConfig)
        {
            _logger = logger;
            configuration = iConfig;
            createDrawer = new ExecutePS(configuration);

        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        [HttpPost]
        public ActionResult Form2(Models.CreateDrawer sm)
        {
            string dbConn2 = ConfigurationManager.AppSettings["MachineName"];
            if (ModelState.IsValid)
            {
                ViewBag.Vid = sm.Vid;
                ViewBag.Name = sm.Name;
                ViewBag.Gtid = sm.Gtid;
                //createDrawer.connect(sm.Vid, sm.Name, sm.Gtid);
                if (createDrawer.connect(sm.Vid, sm.Name, sm.Gtid) == true)
                {
                    ViewBag.Status = "Succesfully Created Drawer";
                }
                else
                {
                    ViewBag.Status = "Failed Creating Drawer. Contact SRE";
                }
                return View("Index");
            }
            else
            {
                ViewBag.Vid = "No Data";
                ViewBag.Name = "No Data";
                ViewBag.Gtid = "No Data";
                return View("Index");
            }

        }
    }
}