﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Management.Automation;
using System.Management.Automation.Runspaces;
using System.Configuration;
using System.Security;
using System.Xml;
using System.IO;
using Microsoft.Extensions.Configuration;
using System.Text;

namespace DrawerCreationVim.Controllers
{
    public class ExecutePS
    {
        private IConfiguration configuration;
        public ExecutePS(IConfiguration iConfig)
        {
            configuration = iConfig;
        }
        public bool connect(String GTID, String AgencyName, String VID)
        {
            //string userName = configuration.GetValue<string>("Connection:Username");
            //string password = configuration.GetValue<string>("Connection:Password");
            //string logFile = configuration.GetValue<string>("Connection:LogFile") + $"{DateTime.Now.ToLongDateString()} {DateTime.Now.ToLongTimeString()}.txt";
            string userName = ConfigurationManager.AppSettings["Username"];
            string password = ConfigurationManager.AppSettings["Password"];
            string logFile = ConfigurationManager.AppSettings["LogFile"];
            string message;
            var securestring = new SecureString();
            foreach (Char c in password)
            {
                securestring.AppendChar(c);
            }
            PSCredential creds = new PSCredential(userName, securestring);
            WSManConnectionInfo connectionInfo = new WSManConnectionInfo();

            connectionInfo.ComputerName = ConfigurationManager.AppSettings["MachineName"];
            connectionInfo.Credential = creds;
            String psProg = File.ReadAllText(@"C:\Test\ProvisionTenant.ps1");
            try
            {
                Runspace runspace = RunspaceFactory.CreateRunspace(connectionInfo);
                runspace.Open();
                using (PowerShell ps = PowerShell.Create())
                {
                    ps.Runspace = runspace;
                    ps.AddScript(psProg);
                    ps.AddArgument(@"$GTID");
                    ps.AddArgument(@"$AgencyName");
                    ps.AddArgument(@"$VID");
                    StringBuilder sb = new StringBuilder();
                    try
                    {
                        var results = ps.Invoke();
                        foreach (var x in results)
                        {
                            sb.AppendLine(x.ToString());
                            // File.AppendAllText(logFile, $"[{DateTime.Now.ToLongDateString()} {DateTime.Now.ToLongTimeString()}]" + x + "\n");
                        }
                    }
                    catch (Exception e)
                    {
                        //problem running script
                        message = e.Message;
                        //File.AppendAllText(logFile, message + "\n");

                    }
                }
                runspace.Close();

                return true;
            }
            catch (Exception e)
            {
                //problem connecting
                return false;
            }



        }
    }
}