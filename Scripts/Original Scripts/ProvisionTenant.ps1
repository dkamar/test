<#
    .SYNOPSIS
    	Used to provision new clients for the shared ImageRight instance.
    
    .DESCRIPTION
    	Connects to an app server and retrieve user permissions.
		
		This script depends on the following files:
			InstallUtil.exe
			DynamicLoader.dll
			ir.ps1
	
	.PARAMETER isNative
		Enables native app server connection. By default, integrated (AD) connection will be used.
			
    .PARAMETER url
		Path to the app server, for example: "tcp://localhost:8082."
	
    .PARAMETER username
		Username to login into the app server, only used with native environment.
	
    .PARAMETER password
		Password to login into the app server, only used with native environment.
	
    .EXAMPLE
    	./Audit-Report.ps1
		
#>
[CmdletBinding(SupportsShouldProcess=$true)]
param
(
												 			[switch]$isNative					= $true,					
															[string]$url 						= "tcp://localhost:8082",	
												 			[string]$username 					= "Admin",		
															[string]$password 					= "V3rt@f0r3",
                                                            [string]$VerbosePreference          = "continue"
                                                            
)

function Clear-Connections()
{	
	[System.Type]::GetType("com.imageright.connection.ConnectionManager, imageright.remoting") | Out-Null
	[System.Type]::GetType("ImageRight.Client.IRSystemRegistry, imageright.client") | Out-Null
	
	([System.Runtime.Remoting.Channels.ChannelServices]::RegisteredChannels) | ForEach-Object {[System.Runtime.Remoting.Channels.ChannelServices]::UnregisterChannel($_)}
	$global:iras = $null
	[ImageRight.Client.IRSystemRegistry]::ClearConnections()
	[com.imageright.connection.ConnectionManager]::ClearAllConnections()
}


function Confirm-Action($yesMessage, $noMessage, $message)
{
	$yes = New-Object System.Management.Automation.Host.ChoiceDescription "&Yes",$yesMessage
	$no = New-Object System.Management.Automation.Host.ChoiceDescription "&No",$noMessage
	$choices = [System.Management.Automation.Host.ChoiceDescription[]]($yes,$no)
	return ($Host.UI.PromptForChoice("",$message,$choices,0))
}

function CleanAndExit()
{
	Write-Verbose "Cleaning up connections ..."
	Clear-Connections
	exit
}

function ProvisionTenant($tenant, $customerName)
{
        
        Write-Verbose "Creating drawer $tenant..."
        Create-Drawer $tenant "Tenant" $customerName
        Write-Verbose "Drawer created successfully...BOOM!"

        $userData = New-Object com.imageright.security.UserData
        $userData.fullName = $customerName
        $userData.flags = [com.imageright.security.UserFlags]::PasswordNeverExpired
        $encoding = [System.Text.Encoding]::UTF8
        [byte[]]$userData.password = $encoding.GetBytes($acctPassword)

        Write-Verbose "Creating User $tenant..."
        Create-User $tenant $customerName $userData
        Write-Verbose "User created successfully...AH YEAH!"

        Write-Verbose "Adding user to groups..."
        $group = "All Users"
        AddUserToGroup $tenant $group
        Write-Verbose "User is a member of $group.  WE'RE ON A ROLL!"


        Write-Verbose "Setting drawer permissions..."

        [com.imageright.security.ObjectAccessPermission[]] $drawerPermissions = [com.imageright.security.ObjectAccessPermission]::read, [com.imageright.security.ObjectAccessPermission]::write,
                                                                                [com.imageright.security.ObjectAccessPermission]::childWriteEnable,[com.imageright.security.ObjectAccessPermission]::childAppendEnable,
                                                                                [com.imageright.security.ObjectAccessPermission]::append
        Set-DrawerPermissions $tenant $tenant $drawerPermissions $null $null
        Write-Verbose "User has permissions to tenant drawer.  BOOYAH!"

        Write-Verbose "Setting instance permissions on drawer $tenant.."
        Set-DrawerInstancePermissionsOnly $tenant
        Write-Verbose "Instance permissions set.  YOU'RE DONE!"
}

function ProvisionByExcel($fullFilePath)
{
    try{
        $results = Get-ExcelData -Path $fullFilePath -Query "SELECT AgencyName, GlobalTenantId from [AMS360Agencies$]"  
    }catch
    {
        Write-Error "Unable to get data from excel file.  Please ensure 'AgencyName' and 'GlobalTenantId' columns exist with column headers."
    }

    foreach($row in $results.Rows)
    {
        try
        {
            Write-Host "Provisioning tenant $($row.GlobalTenantId) $($row.AgencyName)"
            ProvisionTenant $row.GlobalTenantId $row.AgencyName
        }
        catch{
            Write-Error "Error provisioning tenant. $($_)"
        }
    }
}

function Get-ExcelData {
    [CmdletBinding(DefaultParameterSetName='Worksheet')]
    Param(
        [Parameter(Mandatory=$true, Position=0)]
        [String] $Path,

        [Parameter(Position=1, ParameterSetName='Worksheet')]
        [String] $WorksheetName = 'AMS360Agencies-EA',

        [Parameter(Position=1, ParameterSetName='Query')]
        [String] $Query = 'SELECT * FROM [Sheet1$]'
    )

    switch ($pscmdlet.ParameterSetName) {
        'Worksheet' {
            $Query = 'SELECT * FROM [{0}$]' -f $WorksheetName
            break
        }
        'Query' {
            # Make sure the query is in the correct syntax (e.g. 'SELECT * FROM [SheetName$]')
            $Pattern = '.*from\b\s*(?<Table>\w+).*'
            if($Query -match $Pattern) {
                $Query = $Query -replace $Matches.Table, ('[{0}$]' -f $Matches.Table)
            }
        }
    }

    # Create the scriptblock to run in a job
    $JobCode = {
        Param($Path, $Query)

        # Check if the file is XLS or XLSX 
        if ((Get-Item -Path $Path).Extension -eq 'xls') {
            $Provider = 'Microsoft.Jet.OLEDB.4.0'
            $ExtendedProperties = 'Excel 8.0;HDR=YES;IMEX=1'
        } else {
            $Provider = 'Microsoft.ACE.OLEDB.12.0'
            $ExtendedProperties = 'Excel 12.0;HDR=YES'
        }
        
        # Build the connection string and connection object
        $ConnectionString = 'Provider={0};Data Source={1};Extended Properties="{2}"' -f $Provider, $Path, $ExtendedProperties
        $Connection = New-Object System.Data.OleDb.OleDbConnection $ConnectionString

        try {
            # Open the connection to the file, and fill the datatable
            $Connection.Open()
            $Adapter = New-Object -TypeName System.Data.OleDb.OleDbDataAdapter $Query, $Connection
            $DataTable = New-Object System.Data.DataTable
            $Adapter.Fill($DataTable) | Out-Null
        }
        catch {
            # something went wrong 🙁
            Write-Error "Verify expected column headers exist. $($_.Exception.Message)"
        }
        finally {
            # Close the connection
            if ($Connection.State -eq 'Open') {
                $Connection.Close()
            }
        }

        # Return the results as an array
        return ,$DataTable
    }

    # Run the code in a 32bit job, since the provider is 32bit only
    $job = Start-Job $JobCode -RunAs32 -ArgumentList $Path, $Query
    $job | Wait-Job | Receive-Job
    Remove-Job $job
}

try
{
	$location = Split-Path ($MyInvocation.MyCommand.Path)
	Write-Verbose "Changing directory to $location."
	Set-Location $location

	if (-not (Test-Path .\InstallUtil.exe -PathType 'Leaf'))
	{
		throw "Could not find InstallUtil.exe in the directory of this script (`"$location`"). This file is required to register DynamicLoader dll."
	}

	if (-not (Test-Path .\DynamicLoader.dll -PathType 'Leaf'))
	{
		throw "Could not find DynamicLoader.dll in the directory of this script (`"$location`"). This file is required to communicate with the app server."
	}

	Write-Verbose "Installing DynamicLoader.dll."
	.\InstallUtil.exe .\DynamicLoader.dll | Out-Null

	if (-not (Test-Path .\ir.ps1 -PathType 'Leaf'))
	{
		throw "Could not find ir.ps1 in the directory of this script (`"$location`"). This script is required to setup the connection to the app server."
	}

	Write-Verbose "Dot sourcing the ir.ps1 script."
	. .\ir.ps1

	Write-Verbose "Setting up the connection to app server. Native=$isNative ..."
	if ($isNative)
	{	
		#For native environments use the following
		Write-Verbose "Configuring remoting for native security..."
		ir-n | Out-Null	
	}
	else
	{
		#For integrated environments use the following
		Write-Verbose "Configuring remoting for ad security..."
		ir-ad | Out-Null
	}

	Write-Verbose "Connecting to the app server (`"$url`") ..."
	ir-connect $url $username $password | Out-Null
}
catch
{
	throw $_
}

$acctPassword = $null

try
{

    $autoProvision = $false
    if($acctPassword -eq $null)
    {
        $acctPassword = read-host "Enter user account password"
    }

    $provisionType = read-host "Select Provisioning Approach: [1] Provide Excel file or [2] Manually enter tenant id and customer name"
    if($provisionType -eq "1")
    {
        $filepath = read-host "The excel file should contain the GlobalTenantId and AgencyName data with column headers named as such.  Enter full path of provisioning excel"
        if([System.IO.File]::Exists($filepath))
        {
            ProvisionByExcel $filepath
        }
        else
        {
            Write-Host "File does not exist."
        }

    }
    else
    {
        do
        {
            $gtid = read-host "Enter global tenant id"
            $customer = read-host "Enter customer name"
            Write-Host "Provisioning tenant $gtid $customer"
            ProvisionTenant $gtid $customer
        
        }while((($continue = Read-Host "Provision another client? [Y] for Yes, [N] for No")).ToUpper() -eq "Y")
    }
}
catch
{
    $logString = "$(get-date -f s) Error: {0}" -f, $_ 
    Write-Error $_
}
finally
{
    CleanAndExit
}
