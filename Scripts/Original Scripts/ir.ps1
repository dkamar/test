function ql { $args }

if ( @(get-pssnapin imageright -ErrorAction SilentlyContinue).length -eq 0 )
{
  add-pssnapin imageright
}
register-path "$([environment]::GetFolderPath(`"ProgramFiles`"))\imageright\clients"

[void]([System.Reflection.Assembly]::LoadWithPartialName("imageright.client.security"))


function ir-ad 
{
  $fn = "$($env:temp)\irpsh$($pid).config";

@"
<configuration>
  <system.runtime.remoting>
    <application>
      <channels>
        <channel name="ad" ref="tcp" secure="true" tokenImpersonationLevel="Identification" protectionLevel="Sign">
        </channel>
      </channels>
    </application>
  </system.runtime.remoting>
</configuration>
"@ > $fn
	[System.Runtime.Remoting.RemotingConfiguration]::Configure($fn);
   remove-item $fn;
   if ([System.Runtime.Remoting.RemotingConfiguration]::ApplicationName -eq $null) 
   {
      [System.Runtime.Remoting.RemotingConfiguration]::ApplicationName="Powershell";
   }
}

function ir-n 
{ 
  $fn = "$($env:temp)\irpsh$($pid).config";

@"
<configuration>
  <system.runtime.remoting>
    <channelSinkProviders>
      <clientProviders>
        <provider id="native" type="com.imageright.security.remoting.IdentityClientSinkProvider, imageright.remoting.security"/>
      </clientProviders>
    </channelSinkProviders>
    <application>
      <channels>
        <channel ref="tcp" name="tcpNative">
          <clientProviders>
            <formatter ref="binary" />
            <provider ref="native" />
          </clientProviders>
        </channel>
      </channels>
    </application>
  </system.runtime.remoting>
</configuration>
"@ > $fn
	[System.Runtime.Remoting.RemotingConfiguration]::Configure($fn);
   remove-item $fn;
   if ([System.Runtime.Remoting.RemotingConfiguration]::ApplicationName -eq $null) 
   {
      [System.Runtime.Remoting.RemotingConfiguration]::ApplicationName="Powershell";
   }
}

function ir-v ($stsServer="sso.identity.vertafore.com",
			   $stsEmailAppender="identity.vertafore.local",
			   $irVimUrn="urn:WSOL-VSSO.worksmartonline.vertafore.com/ImageRight/")
{ 
  $fn = "$($env:temp)\irpsh$($pid).config";

@"
<configuration>
  <system.runtime.remoting>
    <channelSinkProviders>
      <clientProviders>
        <provider id="claims" type="com.imageright.security.remoting.ClaimsSecurityClientSinkProvider, imageright.remoting.security"/>
      </clientProviders>
    </channelSinkProviders>
    <application>
      <channels>
		<channel ref="tcp" name="tcpClaims">
          <clientProviders>
            <formatter ref="binary" />
            <provider ref="claims" />
          </clientProviders>
        </channel>
        <channel ref="http" name="httpClaims">
          <clientProviders>
            <formatter ref="binary" />
            <provider ref="claims" />
          </clientProviders>
        </channel>
      </channels>
    </application>
	<appSettings>
		<add key="VIM" value="true" />
		<add key="STSServer" value="$stsServer" />
		<add key="STSEmailAppender" value="$stsEmailAppender" />
		<add key="ImagerightServerUrn" value="$irVimUrn" />
	</appSettings>
  </system.runtime.remoting>
</configuration>
"@ > $fn
	[System.Runtime.Remoting.RemotingConfiguration]::Configure($fn);
   remove-item $fn;
   if ([System.Runtime.Remoting.RemotingConfiguration]::ApplicationName -eq $null) 
   {
      [System.Runtime.Remoting.RemotingConfiguration]::ApplicationName="Powershell";
   }
}

function ir-connect ($url, $user, $passwd)
{
	[System.Type]::GetType("com.imageright.connection.ConnectionManager, imageright.remoting");
	[System.Type]::GetType("ImageRight.Client.ImageRightSystem, imageright.client");
	$ci = new-object com.imageright.connection.ConnectionInfo 0,"master",$url;

	$cp = new-object com.imageright.connection.ClaimsCredentialsProvider "","";
	$ci.ClientAuthCallback = $cp;

    $c = [com.imageright.connection.ConnectionManager]::RegisterConnection($ci);

    if (!$c.IntegratedSecurity) 
    {
		if ($user -is [System.Management.Automation.PSCredential]) 
		{
			$user = $user.GetNetworkCredential();
			$passwd = $user.Password; $user = $user.UserName;
		}
		elseif ($passwd -eq $null) 
		{
			$user = (get-credential $user).GetNetworkCredential();
			$passwd = $user.Password; $user = $user.UserName;
		}
		$c.SetCredentials($user, $passwd);
    }
	
	$global:iras=[ImageRight.Client.IRSystemRegistry]::registerConnection($c, $c.GetCurrentCredentials());	
    $Host.UI.RawUI.WindowTitle = "$($Host.UI.RawUI.WindowTitle) $($iras.ServerConnection) $($iras.CurrentUser)"
}

function Get-IRLocation ($name)
{
   $parent = $iras.Files.RootFileContainer;
   $location = $parent;
   if ($name -eq $null) { return $location; }
   if (($name -is [string]) -and ($name.Trim().Length -eq 0)) { return $location; }
   foreach ($i in @($name))
   {
      $location = $parent.GetLocation($i);
      if ($location -eq $null) { throw "Location $i not found in $($parent.Name)"; }
      $parent = $location;
   }
   $location;
}

function Get-Drawer ($name) 
{
   $location = $iras.Files.RootFileContainer;
   $name = @($name);
   for ($i=0; $i -lt $name.Length-1; $i++) 
   {
      $location = $location.GetLocation($name[$i]);
      if ($location -eq $null) { return $null; }
   }
   $drawer = $location.GetDrawer($name[$i]);
   $drawer;
}

function Get-StringTemplate ($values, $op) 
{
   $values = @( $values | % { "$_" } );
   if ( (($op -eq $null) -or ($op -eq "Equal")) -and ( @($values | % { if ($_ -match "[%_]") { $_ } }).Count -gt 0 ) ) 
   { 
      $op = [imageright.objectsearch.templatecondition]"like" 
   }

   $sst = [imageright.objectsearch.filetemplate].GetProperty("FileName").PropertyType;
   if ($op -eq $null) { [string[]]$values -as $sst }
   else 
   { 
      $c = $sst.GetConstructor([type[]]([imageright.objectsearch.templatecondition],[string[]]));
      $op = [imageright.objectsearch.templatecondition]$op;
      $c.Invoke(($op,[string[]]$values));
   }
}

function Get-File ($drawer, $number, $name, $filetype) 
{
   $template = new-object ImageRight.ObjectSearch.FileTemplate (,[long[]]@());
   if ($name -ne $null) { $template.FileName = Get-StringTemplate $name; }
   if ($number -ne $null) 
   {
      $number = @($number);
      if ($number.Length -ge 1) { $template.FileNumber = Get-StringTemplate $number[0]; }
      if ($number.Length -ge 2) { $template.FileNumber2 = Get-StringTemplate $number[1]; }
      if ($number.Length -ge 3) { $template.FileNumber3 = Get-StringTemplate $number[2]; }
   }
   if ($filetype -ne $null) {
      $template.ObjectType = (new-object ImageRight.ObjectSearch.ObjectTypeTemplate (,[long[]]@())).SetName(@($filetype)[0]);
   }
   if ($drawer -ne $null) 
   {
      if ($drawer -is [ImageRight.Client.Drawer])
	  {
         [void]$template.SetDrawer((new-object ImageRight.ObjectSearch.DrawerTemplate $drawer.ID)); 
	  }
	  else 
	  {
	     [Void]$template.SetDrawer((New-Object ImageRight.ObjectSearch.DrawerTemplate (,[long[]]@())).SetLocationName([string]$drawer));
	  }
   }

#   $template;

   $b = $true;
   $iras.Files.FindFile($template, [ref] $b);
}

function New-PermissionData ($allow, $deny, $revoke, $type = "com.imageright.security.objectaccesspermission")
{
   switch ($type)
   {
      "type" { $type = "com.imageright.security.objecttypeaccesspermission"; break; }
      "step" { $type = "com.imageright.server.workflow.StepPermission"; break; }
      "flow" { $type = "com.imageright.server.workflow.WorkflowPermission"; break; }
   }
   $permissiontype = [type]$type;
   $pd = new-object com.imageright.security.PermissionData 0,0;
   foreach ($p in $allow) { $pd.AllowPermissions($p -as $permissiontype); }
   foreach ($p in $deny) { $pd.DenyPermissions($p -as $permissiontype) }
   foreach ($p in $revoke) { $pd.RevokePermissions($p -as $permissiontype) }
   $pd;
}

function Set-DrawerPermissions ($drawer, $account, $allow, $deny, $revoke)
{
   $drawerInstance = Get-Drawer $drawer
   if ($account -isnot [type]"ImageRight.client.interop.isecurityaccount")
   {
      $accname = [string]$account;
      $account = $iras.Security.GetUser([string]$accname);
      if ($account -eq $null) { $account = $iras.Security.GetGroup($accname); }
      if ($account -eq $null) { $account = $iras.Security.GetRole($accname); }
   }
   if ($account -eq $null)
   {
   	  Write-Error "Account $accname is not found";
   }
   else 
   {
      $drawerInstance.ModifyPermissions($account, (new-permissiondata $allow $deny $revoke));
   }
}

function Set-DrawerInstancePermissionsOnly ($drawer)
{
   $drawerInstance = Get-Drawer $drawer
   $drawerInstance.TypeSecurityOverride = $true;
}

function Set-FilePermissions ($file, $account, $allow, $deny, $revoke)
{
   if ($account -isnot [type]"ImageRight.client.interop.isecurityaccount")
   {
      $accname = [string]$account;
      $account = $iras.Security.GetUser([string]$accname);
      if ($account -eq $null) { $account = $iras.Security.GetGroup($accname); }
      if ($account -eq $null) { $account = $iras.Security.GetRole($accname); }
   }
   if ($account -eq $null)
   {
   	  Write-Error "Account $accname is not found";
   }
   else 
   {
      $file.ModifyPermissions($account, (new-permissiondata $allow $deny $revoke));
   }
}

function Set-FlowPermissions ($flow, $account, $allow, $deny, $revoke)
{
   if ($account -isnot [type]"ImageRight.client.interop.isecurityaccount")
   {
      $accname = [string]$account;
      $account = $iras.Security.GetUser([string]$accname);
      if ($account -eq $null) { $account = $iras.Security.GetGroup($accname); }
      if ($account -eq $null) { $account = $iras.Security.GetRole($accname); }
   }
   if ($account -eq $null)
   {
   	  Write-Error "Account $accname is not found";
   }
   else 
   {
      $flow.ModifyPermissions($account, (new-permissiondata $allow $deny $revoke "com.imageright.server.workflow.WorkflowPermission"));
   }
}

function Clear-FilePermissions ($file) { $file.GetAccountList() | % { $file.RemoveAllPermissions($_) }; $file.TypeSecurityOverride = $false; }

function Set-StepPermissions ($step, $account, $allow, $deny, $revoke)
{
   if ($account -isnot [type]"ImageRight.client.interop.isecurityaccount")
   {
      $accname = [string]$account;
      $account = $iras.Security.GetUser([string]$accname);
      if ($account -eq $null) { $account = $iras.Security.GetGroup($accname); }
      if ($account -eq $null) { $account = $iras.Security.GetRole($accname); }
   }
   if ($account -eq $null)
   {
   	  Write-Error "Account $accname is not found";
   }
   else 
   {
      $step.ModifyPermissions($account, (new-permissiondata $allow $deny $revoke "com.imageright.server.workflow.StepPermission"));
   }
}

function Clear-StepPermissions ($step) { $step.GetAccountList() | % { $step.RemoveAllPermissions($_) }; }

function New-IRType ($progid, $name=$progid, $description=$name, $class = "document")
{
   switch ($class)
   {
      { "document" -match "^$_" } { $iras.Metadata.Types.CreateDocumentType($name, $description, $progid) }
      { "file" -match "^$_" } { $iras.Metadata.Types.CreateFileType($name, $description, $progid) }
      { "drawer" -match "^$_" } { $iras.Metadata.Types.CreateDrawerType($name, $description, $progid) }
      { "folder" -match "^$_" } { $iras.Metadata.Types.CreateFolderType($name, $description, $progid) }
      { "location" -match "^$_" } { $iras.Metadata.Types.CreateLocationType($name, $description, $progid) }
      default { throw "Unknown class" }
   }
}

function Get-IRType ($progid, $class)
{
   switch ($class)
   {
      { "document" -match "^$_" } { $iras.Metadata.Types.GetDocumentType($progid) }
      { "file" -match "^$_" } { $iras.Metadata.Types.GetFileType($progid) }
      { "drawer" -match "^$_" } { $iras.Metadata.Types.GetDrawerType($progid) }
      { "folder" -match "^$_" } { $iras.Metadata.Types.GetFolderType($progid) }
      { "location" -match "^$_" } { $iras.Metadata.Types.GetLocationType($progid) }
      default { throw "Unknown class" }
   }
}

function New-IRAttribute ($progid, $name=$progid, $description=$name, $type="atString")
{
   $at = $type -as "com.imageright.server.AttributeType";
   if ($at -eq $null) { $at = "at$type" -as "com.imageright.server.AttributeType" }
   if ($at -eq $null) { throw "Unknown attribute type" }
   $iras.Metadata.AttributeDefinitions.Create($progid,$name,$description,$at);
}

function Get-IRAttribute ($progid="")
{
   $iras.Metadata.AttributeDefinitions | where { $_.Name -match $progid }
}

function Get-Property ($dt, $name) 
{
   $t = [object].getmethod("GetType").Invoke($dt,@())
   $t.getproperty($name).getvalue($dt,@())
}

function Get-PropDef 
{
   $args | % { $pn = $_; @{n="$pn";e=($executioncontext.invokecommand.newscriptblock("get-property `$_ $pn"))} }
}

function New-FolderTemplate ($id, $subfolders=@(), $doctypes=@(), [switch]$repeatable, [switch]$notes, [switch]$calcnotes)
{
   $idi = $id -as [long]
   if ($idi -eq $null) { $idi = Get-Property (get-irtype -cl folder $id) Id }

   $doctypes = @($doctypes | % { $dti = $_ -as [int]; if ($dti -ne $null) { $dti } else { Get-Property (Get-IRType -cl doc $_) Id } })
   
   new-object ImageRight.DataObjects.FolderTemplateDO ([com.imageright.server.longholder]$idi),`
      ([ImageRight.DataObjects.FolderTemplateDO[]]@($subfolders)),`
      ([long[]]@($doctypes)),`
      $repeatable,$notes,$calcnotes
}

function New-FileTemplate ($subfolders=@(), $doctypes=@(), [switch]$notes, [switch]$calcnotes)
{
   $doctypes = @($doctypes | % { $dti = $_ -as [int]; if ($dti -ne $null) { $dti } else { Get-Property (Get-IRType -cl doc $_) Id } })
   
   new-object ImageRight.DataObjects.FileTemplateDO `
      ([ImageRight.DataObjects.FolderTemplateDO[]]@($subfolders)),`
      ([long[]]@($doctypes)),`
      $notes,$calcnotes
}

function Get-Permissions ($obj)
{
   # first get accessors
   $gal = [ImageRight.Client.Security.ISecurableObject].GetMethod("GetAccountList")
   $gp = [ImageRight.Client.Security.ISecurableObject].GetMethod("GetPermissions")
   $gpd = [ImageRight.Client.Security.ISecurableObject].GetMethod("GetPermissionDefinition")

   $pd = $gpd.Invoke($obj, @())
	
   $gal.Invoke($obj,@()) | % { 
        $r=@{} 
    } 
    { 
        $r[$_] = $gp.Invoke($obj,@([ImageRight.Client.Interop.ISecurityAccount]$_)).GetPermissionData() } 
    { $r }
}

function Get-OrphanTasks($workflowIds)
{
    [System.Type]::GetType("com.imageright.server, imageright.server.interfaces");
	[System.Type]::GetType("com.imageright.server.workflow, imageright.server.interfaces");
	$taskCondition = New-Object com.imageright.server.TaskCondition -ArgumentList @(-1, $workflowIds, [com.imageright.server.TaskAttribute]::tsaFlow,[com.imageright.server.CustomAttributeTarget]::catTask, [com.imageright.server.CompareOperation]::coInArray)
    $taskRequest = New-Object com.imageright.server.workflow.TaskRequest 
    $taskRequest.Add($taskCondition);
    $taskTransportObject = $iras.ServerConnection.Workflow.GetOrphanTasks($taskRequest, -1);
    $taskTransportObject;
}

function Get-AllWorkflows()
{
    $wfIdMethod = [com.imageright.server.workflow.WorkflowDef].GetMethod("get_id");
    $workflows = $iras.ServerConnection.WorkflowMetadata.Workflows();
    $workflowIds = New-Object int[] $workflows.length
    $iterator = 0
    $workflows | %{
        $workflowIds[$iterator] = $wfIdMethod.Invoke($_, @());
        $iterator++;
    }
    $workflowIds
}

function Update-TaskAssignment ($taskObj, $securityID)
{   
    $taskidField = [com.imageright.server.workflow.TaskData].GetField("id");
    $taskFileidField = [com.imageright.server.workflow.TaskData].GetField("fileid");
    
    $taskObj | %{
    
        $task = New-Object com.imageright.server.workflow.TaskData
        $task = $_;
        if($task -ne $null)
        {
            $taskid = $taskidField.GetValue($task);
            Write-Verbose ("Route Task ID: {0}" -f,$taskid);
    
            $tp = New-Object com.imageright.server.workflow.TaskParameters;
            $tp.SetAssignedTo($securityID);

            try
            {
                $logString = "$(get-date -f s) Updating task {0}" -f, $taskid
                Add-Content -Path $logfile -Value $logString -Force
                Get-File($taskFileidField.GetValue($task));

                $iras.ServerConnection.Workflow.LockTask($taskid);
                try{
                    $iras.ServerConnection.Workflow.UpdateTask($taskId, $tp);
                }
                catch
                {
                    Write-Error $_
                    $logString = "$(get-date -f s) Error updating task: {0}" -f, $_ 
                    Add-Content -Path $logfile -Value $logString -Force
                    #Get-File($taskFileidField.GetValue($task));
            
                }
                finally
                {
                    $iras.ServerConnection.Workflow.UnlockTask($taskid);  
                }
            }
            catch
            {
                $logString = "$(get-date -f s) Error updating task: {0}" -f, $_ 
                Add-Content -Path $logfile -Value $logString -Force
                #Get-File($taskFileidField.GetValue($task));
                Write-Error $_
            }
        }
    }
}

function Get-File ($taskFileid){
    $taskAry = @($taskFileid)
    $ft = New-Object ImageRight.ObjectSearch.FileTemplate($taskAry);
    try
    {
        $fileList = $iras.ServerConnection.DocumentServer.FindFile($ft);
        if($fileList.objects -ne $null -and $fileList.objects.Length -eq 1)
        {
            $fd = $fileList.objects[0];

            $fileNumberField = [com.imageright.server.FileData].GetField("fileNumber1");
            $fileTypeField = [com.imageright.server.FileData].GetField("objType");
            $drawerField = [com.imageright.server.FileData].GetField("drawer");

            $fileNumber = $fileNumberField.GetValue($fd);
            $fileTypeObj = $fileTypeField.GetValue($fd);

            $objTypeDescField = [com.imageright.server.ObjectTypeData].GetField("name");
            $fileType = $objTypeDescField.GetValue($fileTypeObj);

            $drawerObj = $drawerField.GetValue($fd);
            $locationData = $iras.ServerConnection.DocumentServer.GetObject($drawerObj.Value);
            
            $locationField = [com.imageright.server.LocationData].GetField("name");
            $drawer = $locationField.GetValue($locationData);

            $logString = "$(get-date -f s) Drawer: {0}  FileType: {1}  FileNumber:{2}" -f $drawer, $fileType, $fileNumber
            Add-Content -Path $logfile -Value $logString -Force  
            Write-Verbose $logString
        }
        else
        {
            $logString = "Unable to find file id: {0}" -f, $taskFileid;
        }
    }
    catch
    {
        $logString = "$(get-date -f s) Error getting file information for task {0}: {1}" -f $taskid, $_ 
        Add-Content -Path $logfile -Value $logString -Force  
        Write-Error $_  
    }

}

function Expand-FlagPermissions ($fp, $pd)
{
   $fp = [ImageRight.Client.Security.FlagPermissions]$fp;
   $allowed = @()
   $denied = @()
   $pd | % { if ( $fp.IsAllowed($_) ) { $allowed += $_ }; if ( $fp.IsDenied($_) ) { $denied += $_ } }
   select-object @{N="Allowed";E={$allowed}},@{N="Denied";E={$denied}} -inputObject 1
}

function Flush-Device ($deviceId)
{
	$storageManager = $iras.GetStorageManagerForConnection($iras.ServerConnection)
	$storageManager.FlushDevice($deviceId)
}

function Set-StepPermissions ($step, $account, $allow, $deny, $revoke)
{
   if ($account -isnot [type]"ImageRight.client.interop.isecurityaccount")
   {
      $accname = [string]$account;
      $account = $iras.Security.GetUser([string]$accname);
      if ($account -eq $null) { $account = $iras.Security.GetGroup($accname); }
      if ($account -eq $null) { $account = $iras.Security.GetRole($accname); }
   } 
   $step.ModifyPermissions($account, (new-permissiondata $allow $deny $revoke "step"));
}

# Add MYDESKTOP to user's profile to help resolve case 99372 without code change
# occurs if user has old configuration but uses newly created policy/profile
# requires ADO connection string (can use test udl file to generate)
function AddMyDesktopToAllUsers($dbConnectionString)
{
	if ($dbConnectionString -eq $null)
	{
		Write-Host $("dbConnectionString is required");
		return;
	}
	
	if (Test-Path SUCCESS.txt) { Remove-Item SUCCESS.txt; }
	if (Test-Path ERROR.txt) { Remove-Item ERROR.txt; }

	$conn = New-Object -ComObject ADODB.Connection
	$conn.Open($dbConnectionString)
	$rs = $conn.Execute("select accountid from SecurityAccount where accounttype=0 and accountid>0 order by accountid")
	while(!$rs.eof) {
		$accountid = $rs.Fields.Item("accountid").Value
		$securityId = New-Object com.imageright.security.SecurityID $accountid, ""
		$securityAccount = $iras.Security.GetSecurityAccount($securityId)	
		if (-not $?) { Add-Content -Path ERROR.txt -Value $("Exception when trying to update account with id " + $accountid); }
		if ([string]$securityAccount.AccountType -eq "user") {
			$cp = New-Object com.imageright.configuration.ConfigurationProperties "MYDESKTOP"
			$iras.ServerConnection.LocalConfiguration.SetValues($securityId, [String[]]("DESKTOP"), $cp)		
			if ($?) { Add-Content -Path SUCCESS.txt $("Fixed user's profile with name " + $securityAccount.Name + " and accountid " + $accountid) }
			else { Add-Content -Path ERROR.txt $("Exception when trying to update account with name " + $securityAccount.Name + " and id " + $accountid); }
		}
		$rs.MoveNext()
	}
	
	$conn.Close()
}

function GetACL($dbConnectionString)
{
	[void]([System.Reflection.Assembly]::LoadWithPartialName("imageright.server.base"))

	if ($dbConnectionString -eq $null)
	{
		Write-Host $("dbConnectionString is required");
		return;
	}
	
	$conn = New-Object -ComObject ADODB.Connection
	$conn.Open($dbConnectionString)
	$rs = $conn.Execute("select top 1 accountid, permissions from SecurityAccount")
	while(!$rs.eof) {
		$accountid = $rs.Fields.Item("accountid").Value
		
        $size = $rs.Fields.Item["permissions"].Value
        #$buffer = new-object byte[] -argumentlist $size
        #$buffer = $rs["permissions"].GetChunk($size)
        		
		#$acl = [com.imageright.security.ACL]::LoadFromArray($buffer)

		$rs.MoveNext()
	}
	
	$conn.Close()

	#$list = $acl.GetACEList()
    #Write-Host "ACE List: " + $list.Length
}

function Create-Drawer($name, $type, $description=$name, $path="")
{
    $l = Get-IRLocation $path
    $type = Get-IRType $type -class drawer
    $l.CreateDrawer($type, $name, $description, $null);
}

function Set-FileTemplate ($types, $template)
{
    foreach ($ft in @($types))
    {
        $resolvedType = Get-IRType $ft -class file
        if ($resolvedType -ne $null) 
        {
            $fileTemplate = $resolvedType.GetTemplate()
            $fileTemplate.SetData($template)
            $resolvedType.SetTemplate($fileTemplate)
        }
    }
}

function Create-TestFile($number, [string]$filetypename, [string]$name="Test file $number", $folderMask="", $documentMask="", $drawer=$global:ir_currentDrawer)
{
    $drawer = Resolve-Drawer $drawer
    $fileType = Get-IRType $fileTypename -class file
    if ($fileType -eq $null) { throw "Cannot find file type $filetypename" }
    $fileTemplate = $fileType.GetTemplate()
    
    $file = $drawer.CreateFile($number, $name, "", $fileType.AutomationID)
    Create-Children $file $fileTemplate $folderMask $documentMask
}

function Resolve-Drawer($drawer) 
{
    if ($drawer -eq $null) { throw "Drawer is not specified" }
    if ($drawer -is [ImageRight.Client.Drawer]) { return $drawer; }
    $resolved = Get-Drawer($drawer);
    if ($resolved -eq $null) { throw "Cannot resolve drawer: $drawer"; }
    return $resolved;
}

function Create-Children ($file, $fileTemplate, $folderMask, $documentMask) 
{
    if ($file -eq $null) { return }
    $localTemplate = $fileTemplate.GetFolderTemplate($file.GetTemplatePath())
    if ($localTemplate -eq $null) 
    {
        $localTemplate = $fileTemplate
    }
    foreach ($folderRule in $localTemplate.GetAllowedFolderTypes()) 
    {
        $folderTypeName = Get-Property $folderRule.FolderType AutomationID
        if ($folderTypeName -notmatch $folderMask) { continue }
        if ($folderRule.IsRepeatable) 
        {
            $folder = $file.CreateFolder($folderTypeName, $null, "First instance")
            Create-Children $folder $fileTemplate $folderMask $documentMask
            $folder = $file.CreateFolder($folderTypeName, $null, "Second instance")
            Create-Children $folder $fileTemplate $folderMask $documentMask
        }
        else
        {
            $folder = $file.CreateFolder($folderTypeName, $null, $null)
            Create-Children $folder $fileTemplate $folderMask $documentMask
        }
    }
    foreach ($docType in $localTemplate.GetAllowedDocumentTypes()) 
    {
        $docTypeName = Get-Property $docType AutomationID
        if ($docTypeName -notmatch $documentMask) { continue }
        $document = $file.CreateDocument($docTypeName, $null, "First document")
        Create-Pages $document 3
        $document = $file.CreateDocument($docTypeName, $null, "Second document")
        Create-Pages $document 3
    }
}

#  This function needs a global variable $ir_testfiles in order to work
#  The variable should contain a list of System.IO.FileInfo objects.
#  The list could be obtained by running Get-ChildItem command on a directory
#  that contains your test files
function Create-Pages ($document, $count) 
{
    for ($i=0; $i -lt $count; $i++)
    {
        $testFile = Get-Random $ir_testfiles
        $fs = $testFile.OpenRead()
        try
        {
            [void]$document.AddPage($testFile.Name, $testFile.Extension, $fs, $null)
        }
        finally
        {
          $fs.Close()
        }
    }
}

function FileOwnerShipTransfer($filename, $ToServer)
{
	$files = Get-Content $filename	
	foreach($file in $files)
	{
		$iras.ServerConnection.DocumentServer.MoveOwnership($file, $ToServer)
	}
}

function CheckDocumentHasMissingImages($documentId)
{
	$doc = $iras.GetDocument($documentId)
	return CheckPagesHasMissingImage($doc.Pages)
			-or CheckPagesHasMissingImage($doc.DeletedPages)
			-or CheckPagesHasMissingImage($doc.CutPages)
}

function CheckPagesHasMissingImage($pageCollection)
{
	foreach($page in $pageCollection)
	{
		foreach($version in $page.Versions)
		{
			if (IsImageDataMissing($page.GetVersion($version)))
			{
				return $true
			}
		}
	}
	
	return $false
}

function IsImageDataMissing($iimage)
{
	try
	{
		if ($iimage.Image -eq $null)
		{
			return $true
		}
		
		$imageTypes = $iimage.Image.ImageTypes
		if ($imageTypes -eq $null -or $imageTypes.Length -eq 0)
		{
			return $true
		}
		
		foreach($imageType in $imageTypes)
		{
			$image = $iimage.Image[[int]$imageType]
			if ($image -eq $null)
			{
				return $true
			}
			
			$imageData = $image.Data
			return $imageData -eq $null
		}
	}
	catch [System.Exception]
	{
	}

	return $true
}

function SetCutOffDate($filename, [datetime]$cutoffdate = (Get-Date).Date, $log = "C:\retentionlog.csv")
{
	#Stop Retention Engine
	$iras.GetConnectionRetentionEngine($iras.ServerConnection).Stop()

	if ((Test-Path $filename) -eq $false)
	{
		write-host "File name with IDs cannot be found."
		exit -1;
	}

	set-content $log "`"ID`",`"Status`",`"Error Message`",`"Error Details`""
	$objectIds = Get-Content $filename
	foreach($id in $objectids)
	{
		if (($id -as [long]) -eq $null)
		{
			add-content $log "`"$id`", `"Error`",`"Not a valid ID value`""
		}
		else 
		{
			if (CheckDocumentHasMissingImages($id))
			{
				add-content $log "`"$id`", `"Error`",`"Physical image file may be missing`",`"$error`""
			}
			else
			{
				#Set Cut Off Date
				$dateholder = new-object com.imageright.server.DateTimeHolder($cutoffdate)

				try
				{
					$iras.ServerConnection.DocumentServer.SetCutOffDate($id, $dateholder)
					add-content $log "`"$id`", `"Success`""
				}
				catch [System.Exception]
				{
					add-content $log "`"$id`", `"Errored`", `"Could not set cut off date`", `"$error`""
				}
			}

			$error.clear()
		}
	}
}

function Get-AllTypes()
{
    $iras.ServerConnection.Metadata.Types();
    
}

function Get-Users()
{
    $iras.ServerConnection.Security.Users();
}

function Get-Groups()
{
    $iras.ServerConnection.Security.Groups();
}

function Get-Roles()
{
    $iras.ServerConnection.Security.Roles();
}

function Get-EffectivePermissions($type, $account)
{
    
    $idField = [com.imageright.server.ObjectTypeData].GetMethod("get_id");
    $typeid = $idField.Invoke($type, @());
    
    $ep = $null
    if($account.id.id -ne $null -and $typeid -ne $null)
    {
        $ep = $iras.ServerConnection.Metadata.GetEffectivePermissions($typeid, $account.id.id);
    }
    elseif($account.id.ExternalId -ne $null -and $typeid -ne $null)
    {
        $ep = $iras.ServerConnection.Metadata.GetEffectivePermissions($typeid, $account.id.ExternalId);
    }
    return $ep;
}

function Get-AccessLevel($accountPermssions, $accessLevel)
{
    if(($accountPermssions.allow -band $accessLevel) -eq $accessLevel){ return "Allow"}
    elseif(($accountPermssions.deny -band $accessLevel) -eq $accessLevel){ return "Deny"}
    else{ return ""}
}

function Get-IRClass($type)
{
    $objClassField = [com.imageright.server.ObjectTypeData].GetField("objClass");
    $objClass = $objClassField.GetValue($type);

    $objClassIdMethod = [com.imageright.server.NamedObjectData].GetMethod("get_id");
    $objClassId = $objClassIdMethod.Invoke($objClass, @());

    switch($objClassId)
    {
      { "-6" -match "^$_" } { "Root" }
      { "-5" -match "^$_" } { "LocationType" }  
      { "-4" -match "^$_" } { "DrawerType" }
      { "-3" -match "^$_" } { "FileType" }
      { "-2" -match "^$_" } { "FolderType" }
      { "-1" -match "^$_" } { "DocumentType" }
      default { throw "Unknown class" }    
    }
}

function IIf($If, $IfTrue, $IfFalse) {
    If ($If) {If ($IfTrue -is "ScriptBlock") {&$IfTrue} Else {$IfTrue}}
    Else {If ($IfFalse -is "ScriptBlock") {&$IfFalse} Else {$IfFalse}}
}

function Get-TypeName($type)
{
    $field = [com.imageright.server.ObjectTypeData].GetField("name");
    $typeName = $field.GetValue($type);
    $typeName
}

function Get-UsersInGroup($securityId)
{
    $userList = $null
    $sad = $iras.ServerConnection.Security.GetSecurityAccount($securityId.id);
    if((($sad.accounttype -eq [com.imageright.security.SecurityAccountType]::group) -or
    ($sad.accounttype -eq [com.imageright.security.SecurityAccountType]::role)) -and
    $sad.name -ne "Everyone")
    {
        $userList = $iras.ServerConnection.Security.ListMembers($sad.id);        
    }
    $userList    
}

function Get-SecurityAccount($securityId)
{
    $iras.ServerConnection.Security.GetSecurityAccount($securityId.Id);
}

function Create-User($name, $description, $userData)
{
    $iras.ServerConnection.Security.CreateUser($name, $description, $userData);
}

function AddUserToGroup($accountName, $groupName)
{
   
   $account = $iras.Security.GetUser([string]$accountName);
   
   if ($account -eq $null)
   {
   	  Write-Error "Account $accountName is not found";
      break;
   }
   
   $group = $iras.Security.GetGroup([string]$groupName);
   if ($group -eq $null)
   {
   	  Write-Error "Group $groupName is not found";
      break;
   } 
   
   $group.AddMember($account); 
}

