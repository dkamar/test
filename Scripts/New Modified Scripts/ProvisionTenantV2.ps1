<#
    .SYNOPSIS
    	Used to provision new clients for the shared ImageRight instance.
    
    .DESCRIPTION
    	Connects to an app server and retrieve user permissions.
		
		This script depends on the following files:
			InstallUtil.exe
			DynamicLoader.dll
			ir.ps1
	
	.PARAMETER isNative
		Enables native app server connection. By default, integrated (AD) connection will be used.
			
    .PARAMETER url
		Path to the app server, for example: "tcp://localhost:8082."
	
    .PARAMETER username
		Username to login into the app server, only used with native environment.
	
    .PARAMETER password
		Password to login into the app server, only used with native environment.
	
    .EXAMPLE
    	./Audit-Report.ps1
		
#>
[CmdletBinding(SupportsShouldProcess=$true)]
param
(
															$GTID
															$AgencyName
												 			[switch]$isNative					= $true,					
															[string]$url 						= "tcp://localhost:8082",	
												 			[string]$username 					= "Admin",		
															[string]$password 					= "V3rt@f0r3",
                                                            [string]$VerbosePreference          = "continue"
                                                            
)

function Clear-Connections()
{	
	[System.Type]::GetType("com.imageright.connection.ConnectionManager, imageright.remoting") | Out-Null
	[System.Type]::GetType("ImageRight.Client.IRSystemRegistry, imageright.client") | Out-Null
	
	([System.Runtime.Remoting.Channels.ChannelServices]::RegisteredChannels) | ForEach-Object {[System.Runtime.Remoting.Channels.ChannelServices]::UnregisterChannel($_)}
	$global:iras = $null
	[ImageRight.Client.IRSystemRegistry]::ClearConnections()
	[com.imageright.connection.ConnectionManager]::ClearAllConnections()
}


function Confirm-Action($yesMessage, $noMessage, $message)
{
	$yes = New-Object System.Management.Automation.Host.ChoiceDescription "&Yes",$yesMessage
	$no = New-Object System.Management.Automation.Host.ChoiceDescription "&No",$noMessage
	$choices = [System.Management.Automation.Host.ChoiceDescription[]]($yes,$no)
	return ($Host.UI.PromptForChoice("",$message,$choices,0))
}

function CleanAndExit()
{
	Write-Verbose "Cleaning up connections ..."
	Write-Output "Cleaning up connections ..."
	Clear-Connections
	exit
}

function ProvisionTenant($tenant, $customerName)
{
        
        Write-Verbose "Creating drawer $tenant..."
		Write-Output "Creating drawer $tenant..."
        Create-Drawer $tenant "Tenant" $customerName
        Write-Verbose "Drawer created successfully...BOOM!"
		Write-Output "Drawer created successfully...BOOM!"

        $userData = New-Object com.imageright.security.UserData
        $userData.fullName = $customerName
        $userData.flags = [com.imageright.security.UserFlags]::PasswordNeverExpired
        $encoding = [System.Text.Encoding]::UTF8
        [byte[]]$userData.password = $encoding.GetBytes($acctPassword)

        Write-Verbose "Creating User $tenant..."
		Write-Output "Creating User $tenant..."
        Create-User $tenant $customerName $userData
        Write-Verbose "User created successfully...AH YEAH!"
		Write-Output "User created successfully...AH YEAH!"

        Write-Verbose "Adding user to groups..."
		Write-Output "Adding user to groups..."
        $group = "All Users"
        AddUserToGroup $tenant $group
        Write-Verbose "User is a member of $group.  WE'RE ON A ROLL!"
		Write-Output "User is a member of $group.  WE'RE ON A ROLL!"


        Write-Verbose "Setting drawer permissions..."
		Write-Output "Setting drawer permissions..."

        [com.imageright.security.ObjectAccessPermission[]] $drawerPermissions = [com.imageright.security.ObjectAccessPermission]::read, [com.imageright.security.ObjectAccessPermission]::write,
                                                                                [com.imageright.security.ObjectAccessPermission]::childWriteEnable,[com.imageright.security.ObjectAccessPermission]::childAppendEnable,
                                                                                [com.imageright.security.ObjectAccessPermission]::append
        Set-DrawerPermissions $tenant $tenant $drawerPermissions $null $null
        Write-Verbose "User has permissions to tenant drawer.  BOOYAH!"
		Write-Output "User has permissions to tenant drawer.  BOOYAH!"

        Write-Verbose "Setting instance permissions on drawer $tenant.."
		Write-Output "Setting instance permissions on drawer $tenant.."
        Set-DrawerInstancePermissionsOnly $tenant
        Write-Verbose "Instance permissions set.  YOU'RE DONE!"
		Write-Output "Instance permissions set.  YOU'RE DONE!"
}



try
{
	$location = Split-Path ($MyInvocation.MyCommand.Path)
	Write-Verbose "Changing directory to $location."
	Set-Location $location

	if (-not (Test-Path .\InstallUtil.exe -PathType 'Leaf'))
	{
		throw "Could not find InstallUtil.exe in the directory of this script (`"$location`"). This file is required to register DynamicLoader dll."
	}

	if (-not (Test-Path .\DynamicLoader.dll -PathType 'Leaf'))
	{
		throw "Could not find DynamicLoader.dll in the directory of this script (`"$location`"). This file is required to communicate with the app server."
	}

	Write-Verbose "Installing DynamicLoader.dll."
	.\InstallUtil.exe .\DynamicLoader.dll | Out-Null

	if (-not (Test-Path .\ir.ps1 -PathType 'Leaf'))
	{
		throw "Could not find ir.ps1 in the directory of this script (`"$location`"). This script is required to setup the connection to the app server."
	}

	Write-Verbose "Dot sourcing the ir.ps1 script."
	. .\irv2.ps1

	Write-Verbose "Setting up the connection to app server. Native=$isNative ..."
	if ($isNative)
	{	
		#For native environments use the following
		Write-Verbose "Configuring remoting for native security..."
		ir-n | Out-Null	
	}
	else
	{
		#For integrated environments use the following
		Write-Verbose "Configuring remoting for ad security..."
		ir-ad | Out-Null
	}

	Write-Verbose "Connecting to the app server (`"$url`") ..."
	ir-connect $url $username $password | Out-Null
}
catch
{
	throw $_
}

$acctPassword = "v41Rmti@ws" 

try
{
    $autoProvision = $false
    
    Write-Host "Provisioning tenant $gtid $customer"
	Write-Output "Provisioning tenant $gtid $customer"
    ProvisionTenant $GTID $AgencyName
        
        }while((($continue = Read-Host "Provision another client? [Y] for Yes, [N] for No")).ToUpper() -eq "Y")
    }
}
catch
{
    $logString = "$(get-date -f s) Error: {0}" -f, $_ 
    Write-Error $_
}
finally
{
    CleanAndExit
}
