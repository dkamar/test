# Drawer Creation Tool
-This Project is supposed to simplify the process of creating shared drawers for edocs. 
Its a website that is hosted on an EC2 on AWS and will only be accessable on the vertafore network. It will use VIM to authenticate the users.
It is executing the powershell script used to create the drawers in the background.
Orignal Scripts and Modfied Scripts are included in this repo
http://wiki.vertafore.com/display/TEM/SRE+Infosphere+-+eDocs+Support

EC2Information:

EC2 script needs to be executed on:	
- i-03787fdae9ce2f7bb (ImageRight-eDocs-Core-Prod)
- IP to RDP into: 10.144.11.205(Only works if machine is on the same network as this EC2, so RDP from the Machine below into this one or use the 	one for manually created drawers)
- Username IMAGERIGHT-SHAR\Administrator -Pass Xr7Ag0F&*D&q21
 
EC2 App should be hosted on:
- i-0d2b7d581f8a8471a (SharedDrawerTool)
- IP to RDP into: 44.200.243.72
- Credentials:
	public ip = 44.200.243.72 Username = .\admin Password =Vertafore1

Workflow

- User visits website

	
- Website will validate if user has correct permissions
	
- User fills in required information(VID,GTID, Angency Name)
	
- User hits submit(Basic Validation on the 3 fields)
	- App will take that information and execute the modified Powerscript on i-03787fdae9ce2f7bb(10.144.11.205) to create the drawer
- Website will report if it succeeded or if an error occured

Whats done:
- Logic Complete
	
What needs to be done:
- Needs to be deployed on EC2
- Needs to be tested with test agencies(talk to Nellie Massoni to point you to the right people in support)
